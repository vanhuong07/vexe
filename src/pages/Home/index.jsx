import React, { Component } from 'react'
import {Typography, Grid, Container, Button} from '@material-ui/core'
import SeatList from '../../components/SeatList';
import SeatReser from '../../components/SeatReser';


class Home extends Component {
    render() {
        return (
            <div>
                <Typography>
                    Đặt vé xe Bus Hãng CyberSoft
                </Typography>
                <Container maxWidth="lg">
                    <Grid container spacing={3}>
                        <Grid item xs={6}>
                            <Button variant="outlined" disabled > 
                                Tài xế
                            </Button>
                            <SeatList/>
                        </Grid>
                        <Grid item xs={6}>
                            <Typography>
                                Danh sách ghế đã đặt ()
                            </Typography>
                            <SeatReser />
                        </Grid> 
                    </Grid>
                </Container>
            </div>
        )
    }
}


export default Home;