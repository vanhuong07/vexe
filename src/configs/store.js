import {createStore , combineReducers, compose, applyMiddleware } from 'redux'
import seat from '../redux/reducer/seat'
import thunk from 'redux-thunk'

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const reducer = combineReducers({
    seat,

})

export const store = createStore(
    reducer,
    composeEnhancers(applyMiddleware(thunk))
  );
  