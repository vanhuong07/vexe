import React, { Component } from 'react'
import {Box, withStyles} from '@material-ui/core'
import {connect} from 'react-redux'
import SeatItem from '../SeatItem';
import {style} from './style';
import {createAction} from '../../redux/action/index'
import { SHOW_SEATRESE } from '../../redux/action/type';

const defaultProps = {
    bgcolor: 'background.paper',
    borderColor: 'text.primary',
    m: 1,
    border: 1,
    style: { width: '3rem', height: '3rem' },
  };

class SeatList extends Component {
    handleSubmit = () => {
        if(this.props.seatlist){
            this.props.dispatch(createAction(SHOW_SEATRESE, this.seatlist))
        }
    }
    renderSeatList = () => {
        return (this.props.seatlist.map((item,key) => {
            return (
                <Box borderRadius={16} {...defaultProps} key={key}  display="inline-block" className={this.props.classes.SeatItem} onClick={this.handleSubmit}>
                    <SeatItem item={item} />
                </Box>
            )
        }))
    }
    render() {
        return (
            <div>
                <div className="box">
                    {this.renderSeatList()}
                </div>
                {/* <Box borderRadius={16} {...defaultProps} > abc </Box> */}
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return({
        seatlist: state.seat.seatList,
    })
}


export default connect(mapStateToProps)(withStyles(style)(SeatList));
